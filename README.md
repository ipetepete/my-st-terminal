### Gauge Krahe's build of simple terminal with the following patches applied:

- Alpha

- Bold Is Not Bright

- Boxdraw

- Disable Bold, Italic, and Roman fonts

- Externalpipe

- iso14755

- Scrollback

- Scrollback-Mouse

- Xresources
